﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Web;
using System.Net;
using Newtonsoft.Json;
using Xamarin.Essentials;
using System.Text.RegularExpressions;

namespace DIctionary
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public class WordClass
    {
        public string Word { get; set; }
        public string Type { get; set; }
        public string Definition { get; set; }
        public string Example { get; set; }
    }
    public partial class MainPage : ContentPage
    {
        public String token = "163227ce1d482c082c4fa11f502e4f5eb22ae846";
        public MainPage()
        {
            InitializeComponent();
        }
        void Entry_Completed(object sender, EventArgs e)
        {
            var current = Connectivity.NetworkAccess;

            if (current != NetworkAccess.Internet) // check internet access
            {
                errorMsg.Text = "No internet access.";
                defListView.ItemsSource = null;
                return;
            }
            var text = ((Entry)sender).Text; // cast sender to access the properties of the Entry
            if (text == "" || text == null)
            {
                errorMsg.Text = "Please enter a word.";
                defListView.ItemsSource = null;
                return;
            }
            Regex r = new Regex("^[a-zA-Z0-9]*$");
            if (!r.IsMatch(text)) // check if the entry string is alphanumerical or not
            {
                errorMsg.Text = "Only alphanumerical characters are allowed.";
                defListView.ItemsSource = null;
                return;
            }
            var URL = new UriBuilder("https://owlbot.info/api/v4/dictionary/" + text); // create the URL with the Entry text

            var client = new WebClient();
            client.Headers.Add("Authorization", "Token " + this.token); // add authorization header with token
            try
            {
                errorMsg.Text = ""; // reset the error msg in case there was one
                displayResults(client.DownloadString(URL.ToString()), text); // make the http request and display the result
            } catch (System.Net.WebException)
            {
                errorMsg.Text = "Word not found.";
                defListView.ItemsSource = null;
            }
        }

        void displayResults(string results, string query)
        {
            dynamic jsonResults = JsonConvert.DeserializeObject(results); // convert result string to json object
            var words = jsonResults.definitions;
            List<WordClass> wordList = new List<WordClass>();
            foreach (var word in words) // create a custom list from the json object
            {
                wordList.Add(new WordClass()
                {
                    Word = query,
                    Type = word.type,
                    Definition = word.definition,
                    Example = word.example,
                });
            }
            defListView.ItemsSource = wordList; // assign the list to the xaml control to display it
        }
    }
}
